# Taxonomy Widget Plus

## Table of contents

- Introduction
- Requirements
- Installation
- Usage
- Current Maintainers


## Introduction

This module provides a custom widget for taxonomy term reference field that:
- Display as a tree-like structure.
- Allow adding new term directly in the entity form.
- Able to be placed in sidebar instead of main form.


## Requirements

This module requires Drupal 8.8 or later. There is no external dependency.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see _[Installing Drupal Modules]_.

[Installing Drupal Modules]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules


## Usage

1. In "Manage form display", select "Taxonomy Widget Plus"
for your taxonomy term referenced field(s).
2. There are 4 options you can use to customize the behavior of the widget:
    - Place field in sidebar
    - Collapsed by default
    - Use scrollbar
    - Hide parent field
3. Save the config and you're ready!


## Current Maintainers

- Bao Tran - [@dinh-bao-tran](https://www.drupal.org/u/dinh-bao-tran)

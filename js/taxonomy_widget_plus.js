(function($, Drupal) {

  /**
   * Attaches the tree behavior to the widget form.
   */
  Drupal.behaviors.taxonomyTreeWidget = {
    attach: function(context, settings) {
      for (const [link_id, wrapper_id] of Object.entries(settings.taxonomyTreeWidget.addNewButtonMapping)) {
        $(`#${link_id}`).click(function(e) {
          e.preventDefault();
          $(`#${wrapper_id}`).removeClass('hidden');
        });
      }

      const wrapper_id = settings.taxonomyTreeWidget.newTermWrapperId;
      const new_term_id = settings.taxonomyTreeWidget.newTermId;
      if (wrapper_id) {
        $(`#${wrapper_id}`).find(`input[value="${new_term_id}"]`).focus();
      }
    }
  }

})(jQuery, Drupal);
<?php

namespace Drupal\taxonomy_widget_plus\Element;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Url;
use Drupal\taxonomy_widget_plus\TaxonomyTreeBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form element for taxonomy term tree.
 *
 * @FormElement("taxonomy_tree")
 */
class TaxonomyTree extends FormElement implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The taxonomy tree builder.
   *
   * @var \Drupal\taxonomy_widget_plus\TaxonomyTreeBuilderInterface
   */
  protected $treeBuilder;

  /**
   * Constructs a new BreakLockLink.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The current user.
   * @param \Drupal\taxonomy_widget_plus\TaxonomyTreeBuilderInterface $tree_builder
   *   The tree builder.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    TaxonomyTreeBuilderInterface $tree_builder
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->treeBuilder = $tree_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('taxonomy_widget_plus.tree_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'taxonomy_tree',
      '#input' => TRUE,
      '#tree' => TRUE,
      '#process' => [
        [
          static::class,
          'processGroup',
        ],
        [
          $this,
          'processTaxonomyTree',
        ],
      ],
      '#pre_render' => [
        [
          static::class,
          'preRenderGroup',
        ],
        [
          static::class,
          'preRenderTaxonomyTree',
        ],
      ],
      '#default_value' => [],
      '#attached' => [
        'library' => [
          'taxonomy_widget_plus/taxonomy_widget_plus',
        ],
      ],
    ];
  }

  /**
   * Prepare the attributes for taxonomy tree wrapper.
   */
  public static function preRenderTaxonomyTree($element) {
    $element['#theme_wrappers']['container']['#id'] = $element['#id'] . '--wrapper';

    $add_new_link_id = $element['add_new_link']['#id'];
    $add_new_wrapper_id = $element['add_new']['#id'];
    $element['#attached']['drupalSettings']['taxonomyTreeWidget']['addNewButtonMapping'][$add_new_link_id] = $add_new_wrapper_id;

    return $element;
  }

  /**
   * Processes a taxonomy tree form element.
   */
  public function processTaxonomyTree(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = is_array($element['#value']) ? $element['#value'] : [];
    $options_tree = [];

    foreach ($element['#vocabularies'] as $vocabulary) {
      $options = $this->treeBuilder
        ->createTermHierarchy(0, $vocabulary->id(), '', $value);
      $options_tree = array_merge($options_tree, $options);
    }

    $element['#options_tree'] = $options_tree;
    $element['#options'] = $this->treeBuilder
      ->convertTermsToOptions($element['#options_tree']);
    $terms = !empty($element['#options_tree']) ? $element['#options_tree'] : [];

    $tree = new \stdClass();
    $tree->children = $terms;
    $element[] = $this->treeBuilder
      ->buildTreeRoot($element, $tree, $value, $element['#cardinality'], [], 1);

    // Provide an "Add new" utility.
    // @todo find a better alternative for loadTree()
    // which can be very large
    $taxonomy_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $options = ['<' . $this->t('root') . '>'];
    foreach ($element['#vocabularies'] as $vocabulary) {
      $tree = $taxonomy_storage->loadTree($vocabulary->id());
      foreach ($tree as $item) {
        $options[$item->tid] = str_repeat('-', $item->depth) . $item->name;
      }
    }

    $has_create_access = $this->checkCreateAccess($element['#vocabularies']);

    $element['add_new_link'] = [
      '#type' => 'link',
      '#title' => $this->t('+ Add new'),
      '#url' => Url::fromUserInput('#add-new-term'),
      '#access' => $has_create_access,
    ];

    $element['add_new'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'hidden',
      ],
      '#access' => $has_create_access,
    ];
    $element['add_new']['term_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
    ];
    $element['add_new']['parent'] = [
      '#type' => 'select',
      '#title' => $this->t('Parent'),
      '#options' => $options,
      '#default_value' => 0,
      '#access' => !$element['#hide_parent_field'],
    ];

    $element['add_new']['submit'] = [
      '#type' => 'submit',
      // @see https://www.drupal.org/node/2546700#comment-11164091
      '#value' => $this->t('Add @term', ['@term' => $element['#title']]),
      '#button_type' => 'primary',
      '#limit_validation_errors' => [
        array_merge($element['#parents'], ['add_new']),
      ],
      '#submit' => [[$this, 'addNewAjaxSubmit']],
      '#ajax' => [
        'callback' => [self::class, 'addNewAjaxCallback'],
        'event' => 'click',
        'wrapper' => $element['#id'] . '--wrapper',
      ],
    ];

    $element['#theme_wrappers']['container'] = [
      '#attributes' => [
        'class' => 'taxonomy-tree-container',
      ],
    ];

    $element['#cache']['#tags'][] = 'taxonomy_term_list';
    $element['#cache']['#contexts'][] = 'user';

    // Remove default as we use custom validation on the widget.
    unset($element['#needs_validation']);

    return $element;
  }

  /**
   * Ajax submit handler for "Add new" button.
   */
  public function addNewAjaxSubmit(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#parents'];
    array_pop($parents);
    $field_name = reset($parents);
    $element = $form[$field_name]['widget'];

    $term_name = $form_state->getValue(array_merge($parents, ['term_name']), '');
    $parent_id = $form_state->getValue(array_merge($parents, ['parent']), 0);

    if (!empty($term_name)) {
      $taxonomy_storage = $this->entityTypeManager->getStorage('taxonomy_term');

      if (!empty($parent_id)) {
        $parent_term = $taxonomy_storage->load($parent_id);
        $vid = $parent_term->bundle();
      }
      else {
        $vocabulary = reset($element['#vocabularies']);
        $vid = $vocabulary->id();
      }

      $new_term = $taxonomy_storage->create([
        'name' => $term_name,
        'vid' => $vid,
        'parent' => $parent_id,
      ]);
      $new_term->save();
      $form_state->set('new_term_id', $new_term->id());
    }
    else {
      // Avoids highlighting wrong option.
      $form_state->set('new_term_id', '');
    }

    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback for "Add new" button.
   */
  public static function addNewAjaxCallback(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#parents'];
    array_pop($parents);
    $field_name = reset($parents);
    $element = $form[$field_name]['widget'];

    // Send the info of newly added term for js to highlight.
    $new_term_id = $form_state->get('new_term_id');
    $new_term_wrapper_id = $new_term_id ? $element['#id'] : '';
    $element['#attached']['drupalSettings']['taxonomyTreeWidget']['newTermId'] = $new_term_id;
    $element['#attached']['drupalSettings']['taxonomyTreeWidget']['newTermWrapperId'] = $new_term_wrapper_id;

    // Clean the submited values.
    $element['add_new']['term_name']['#value'] = '';
    $element['add_new']['parent']['#value'] = '0';
    $element['#theme_wrappers']['details']['#attributes']['open'] = TRUE;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $value = [];
    $element += [
      '#default_value' => [],
    ];

    foreach ($element['#default_value'] as $key) {
      if (isset($key['target_id'])) {
        $value[$key['target_id']] = $key['target_id'];
      }
    }

    return $value;
  }

  /**
   * Check whether the current user can add new term for the vocabularies.
   *
   * @return bool
   *   TRUE if the user has create access for the vocabularies, FALSE otherwise.
   *
   * @todo If a field accepts multiple vocabularies, we should allow the user to
   * create new term of the vocabulary(es) that they do have create access for.
   */
  protected function checkCreateAccess(array $vocabularies) {
    $access_control_handler = $this->entityTypeManager
      ->getAccessControlHandler('taxonomy_term');

    foreach ($vocabularies as $vocabulary) {
      if (!$access_control_handler->createAccess($vocabulary->id())) {
        return FALSE;
      }
    }

    return TRUE;
  }

}

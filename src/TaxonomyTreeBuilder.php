<?php

namespace Drupal\taxonomy_widget_plus;

use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Default taxonony tree builder implementation.
 */
class TaxonomyTreeBuilder implements TaxonomyTreeBuilderInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a WebformEmailProvider object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The configuration object factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    LanguageManagerInterface $language_manager,
    Connection $database
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->languageManager = $language_manager;
    $this->database = $database;
  }

  /**
   * {@inheritDoc}
   */
  public function createTermHierarchy($tid, $vid, $label, $default = []) {
    $terms = $this->getTermChildren($tid, $vid);
    $result = [];

    foreach ($terms as &$term) {
      $children = $this->createTermHierarchy($term->tid, $vid, $label, $default);
      if (is_array($children)) {
        $term->children = $children;
        $term->children_selected = $this->hasSelectedChiren($term, $default);
      }
      else {
        $term->children_selected = FALSE;
      }
      array_push($result, $term);
    }

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function convertTermsToOptions(array $terms) {
    $options = [];

    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
      $options += $this->convertTermsToOptions($term->children);
    }

    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function buildTreeRoot($element, $term, $value, $cardinality, $parent_tids, $depth) {
    $leaves_only = FALSE;
    $container = [
      '#theme' => 'taxonomy_tree_root',
      '#cardinality' => $cardinality,
      '#leaves_only' => $leaves_only,
      '#depth' => $depth,
    ];

    foreach ($term->children as $child) {
      $container[$child->tid] = $this->buildTreeItem($element, $child, $value, $cardinality, $parent_tids, $container, $depth);
    }

    return $container;
  }

  /**
   * {@inheritDoc}
   */
  public function buildTreeItem($element, $term, $value, $cardinality, $parent_tids, $parent, $depth) {
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $leaves_only = FALSE;

    // @todo improve this: don't load term directly.
    /** @var \Drupal\taxonomy\TermInterface $term_object */
    $term_object = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->load($term->tid);

    if ($this->moduleHandler->moduleExists('locale')) {
      if ($term_object->hasTranslation($langcode)) {
        $term_name = $term_object->getTranslation($langcode)->label();
      }
    }

    if (empty($term_name)) {
      $term_name = $term->name;
    }

    $container = [
      '#theme' => 'taxonomy_tree_item',
      '#cardinality' => $cardinality,
      '#leaves_only' => $leaves_only,
      '#term_name' => $term_name,
      '#depth' => $depth,
    ];

    if (empty($element['#leaves_only']) || count($term->children) == 0) {
      $leaf_element = [
        '#type' => ($cardinality == 1) ? 'radio' : 'checkbox',
        '#title' => $term_name,
        '#on_value' => $term->tid,
        '#off_value' => 0,
        '#return_value' => $term->tid,
        '#parent_values' => $parent_tids,
        '#default_value' => isset($value[$term->tid]) ? $term->tid : NULL,
        '#attributes' => $element['#attributes'] ?? NULL,
        '#ajax' => $element['#ajax'] ?? NULL,
      ];

      if ($cardinality == 1) {
        $parents_for_id = array_merge($element['#parents'], [$term->tid]);
        $leaf_element['#id'] = Html::getId('edit-' . implode('-', $parents_for_id));
        $leaf_element['#parents'] = array_merge($element['#parents'], ['widget']);
      }

      if ($element['#description_on_hover'] && !$term_object->get('description')->isEmpty()) {
        $leaf_element['#label_attributes'] = [
          'title' => strip_tags($term_object->getDescription()),
        ];
      }

      $container[$term->tid] = $leaf_element;
    }

    if (property_exists($term, 'children') && count($term->children) > 0) {
      $parents = $parent_tids;
      $parents[] = $term->tid;
      $container[$term->tid . '-children'] = $this
        ->buildTreeRoot($element, $term, $value, $cardinality, $parents, $depth + 1);
    }

    return $container;
  }

  /**
   * Check if this term has a selected child.
   *
   * @return bool
   *   TRUE if the term has a selected child, FALSE otherwise.
   */
  private function hasSelectedChiren($term, $default) {
    foreach ($term->children as $child) {
      if (isset($default[$child->tid]) || $child->children_selected) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Gets the children list of a term.
   *
   * @return array
   *   The children list as an array.
   */
  private function getTermChildren(int $tid, string $vid) {
    $query = $this->database->select('taxonomy_term_field_data', 't');
    $query->join('taxonomy_term__parent', 'p', 't.tid = p.entity_id');
    $query->fields('t', ['tid', 'name']);
    $query->addField('t', 'vid', 'vocabulary_machine_name');
    $query
      ->condition('t.vid', $vid)
      ->condition('p.parent_target_id', (string) $tid)
      ->addTag('term_access')
      ->addTag('translatable')
      ->orderBy('t.weight')
      ->orderBy('t.name');

    $result = $query->execute();
    $terms = [];

    while ($term = $result->fetchObject()) {
      $terms[$term->tid] = $term;
    }

    return $terms;
  }

}

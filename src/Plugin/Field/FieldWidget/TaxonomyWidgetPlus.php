<?php

namespace Drupal\taxonomy_widget_plus\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'taxonomy_widget_plus' widget.
 *
 * @FieldWidget(
 *   id = "taxonomy_widget_plus",
 *   label = @Translation("Taxonomy Widget Plus"),
 *   field_types = { "entity_reference" },
 *   multiple_values = TRUE
 * )
 */
class TaxonomyWidgetPlus extends WidgetBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'description_on_hover' => TRUE,
      'sidebar' => TRUE,
      'use_scrollbar' => TRUE,
      'start_collapsed' => FALSE,
      'hide_parent_field' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $storage = $field_definition->getFieldStorageDefinition();
    return $storage->getSetting('target_type') === 'taxonomy_term';
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['description_on_hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Description on hover'),
      '#description' => $this->t('Show term description when hover over the term name.'),
      '#default_value' => $this->getSetting('description_on_hover'),
    ];

    $form['sidebar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place field in sidebar'),
      '#description' => $this->t('If checked, this field will be placed in the sidebar of the entity form.'),
      '#default_value' => $this->getSetting('sidebar'),
    ];

    $form['use_scrollbar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use scrollbar'),
      '#description' => $this->t("Display a vertical scroll when there is a long list of terms."),
      '#default_value' => $this->getSetting('use_scrollbar'),
    ];

    $form['start_collapsed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Collapsed by default'),
      '#description' => $this->t('Show the field as collapsed on first load.'),
      '#default_value' => $this->getSetting('start_collapsed'),
    ];

    $form['hide_parent_field'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide parent field'),
      '#description' => $this->t("Hide parent field when adding new term (not applicable for multi-vocabularies field)."),
      '#default_value' => $this->getSetting('hide_parent_field'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($this->getSetting('description_on_hover')) {
      $summary[] = $this->t('Show description on hover');
    }

    if ($this->getSetting('sidebar')) {
      $summary[] = $this->t('Placed in sidebar');
    }

    if ($this->getSetting('use_scrollbar')) {
      $summary[] = $this->t('Use scrollbar for long list');
    }

    if ($this->getSetting('start_collapsed')) {
      $summary[] = $this->t('Start collapsed');
    }

    if ($this->getSetting('hide_parent_field')) {
      $summary[] = $this->t('Hide parent field when adding new term');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $handler_settings = $this->getFieldSetting('handler_settings');
    $vocabularies = $this->entityTypeManager
      ->getStorage('taxonomy_vocabulary')
      ->loadMultiple($handler_settings['target_bundles']);

    $element += [
      '#type' => 'taxonomy_tree',
      '#title_display' => 'invisible',
      '#default_value' => $items->getValue(),
      '#vocabularies' => $vocabularies,
      '#cardinality' => $this->fieldDefinition->getFieldStorageDefinition()->getCardinality(),
      '#value_key' => 'target_id',
      '#description_on_hover' => $this->getSetting('description_on_hover'),
      '#start_collapsed' => $this->getSetting('start_collapsed'),
      '#use_scrollbar' => $this->getSetting('use_scrollbar'),
      '#hide_parent_field' => $this->getSetting('hide_parent_field'),
    ];

    $start_collapsed = $element['#start_collapsed'];
    $element['#theme_wrappers'] = [
      'details' => [
        '#attributes' => [
          'open' => !$start_collapsed,
        ],
        // @see https://www.drupal.org/project/drupal/issues/2998194
        '#summary_attributes' => [],
        '#value' => NULL,
      ],
    ];

    $sidebar = $this->getSetting('sidebar');
    if ($sidebar) {
      $element['#group'] = 'advanced';
      $element['#weight'] = 100;
    }

    $element['#element_validate'] = [
      [
        static::class,
        'validateTaxonomyTree',
      ],
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $flattened_values = [];

    // Processing for radio buttons.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() === 1) {
      $selected = $values['widget'];
      $flattened_values[$selected] = $selected;
    }
    else {
      // @todo figure out a cleaner massaging method.
      if (isset($values[0]) && is_array($values[0])) {
        array_walk_recursive($values[0], function ($value, $key) use (&$flattened_values) {
          $flattened_values[$key] = $value;
        });
      }
      else {
        $flattened_values = $values;
      }

      $flattened_values = array_filter($flattened_values, 'is_numeric', ARRAY_FILTER_USE_KEY);
      $flattened_values = array_filter($flattened_values);
    }

    return $flattened_values;
  }

  /**
   * Form validation handler for taxonomy tree element.
   */
  public static function validateTaxonomyTree(array &$element, FormStateInterface $form_state, &$complete_form) {
    $values = $element['#value'];
    if ($element['#cardinality'] != -1 && count($values) > $element['#cardinality']) {
      $form_state->setError($element, t('%name: this field cannot hold more than @count values.', [
        '%name' => $element['#title'],
        '@count' => $element['#cardinality'],
      ]));
    }
  }

}

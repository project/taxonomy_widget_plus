<?php

namespace Drupal\taxonomy_widget_plus;

/**
 * Interface for taxonomy tree buidlers.
 */
interface TaxonomyTreeBuilderInterface {

  /**
   * Returns a taxonomy term hierarchy in a nested array.
   */
  public function createTermHierarchy($tid, $vid, $label, $default = []);

  /**
   * Converts a list of terms to a list of ptions.
   */
  public function convertTermsToOptions(array $terms);

  /**
   * Builds a tree root starting from an element.
   */
  public function buildTreeRoot($element, $term, $value, $cardinality, $parent_tids, $depth);

  /**
   * Builds a single item of the taxonomy tree.
   */
  public function buildTreeItem($element, $term, $value, $cardinality, $parent_tids, $parent, $depth);

}
